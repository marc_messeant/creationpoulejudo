/*
 * ClasseAge.java
 *
 * Created on 27 octobre 2006, 14:51
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */

package tournoijudo;
import java.util.*;
import java.io.*;
import jxl.*;
import jxl.write.*;

/**
 *
 * @author Marc
 */
public class Categorie implements Comparable,Serializable{
    private int anneeMin,anneeMax;
    private String sexe;
    private String nom;
    private String nom2; // deuxième nom de la catégorie (pour les catégories avec poule)
    private float poidsMax,poidsMin;
    private Participants participantsParPoids = new Participants();
    private Participants participantsParPoules = new Participants();
    private Participants participantsAClasser = new Participants();
    public int taillePoule = 4;
    public int taillePouleMax = 5;
    public float tolerancePoids = 0.1F;
    public int toleranceClub = 4;
    
    /** Creates a new instance of Categorie */
    public Categorie(String nom,String sexe,int anneeMin,int anneeMax,float poidsMax,float poidsMin, int taillePoule, int taillePouleMax, float tolerancePoids, int toleranceClub,String nom2) {
        this.anneeMin = anneeMin;
        this.anneeMax = anneeMax;
        this.sexe = sexe;
        this.poidsMax = poidsMax;
        this.poidsMin = poidsMin;
        this.taillePoule = taillePoule;
        this.taillePouleMax = taillePouleMax;
        this.tolerancePoids = tolerancePoids;
        this.toleranceClub = toleranceClub;
        this.nom = nom;
        this.nom2 = nom2;
    }
    public String getSexe() {
        return(sexe);
    }
    public String getNom() {
        return(nom);
    }
    public String getNom2() {
        return(nom2);
    }
    public void setPoidsMin(float poids) {
        poidsMin = poids;
    }
    public float getPoidsMax() {
        return poidsMax;
    }
    
    public float getPoidsMin() {
        return poidsMin;
    }
    
    public String toString() {
        return(nom);
    }
    public int compareTo(Object o) {
        Categorie cat1 = (Categorie)o;
        if (cat1.getSexe().compareTo(this.getSexe()) != 0) return(cat1.getSexe().compareTo(this.getSexe()));
        if (cat1.getAnneeMin() > getAnneeMax()) return(-1);
        if (cat1.getAnneeMax() < getAnneeMin()) return(1);
        if (cat1.getPoidsMax() > getPoidsMax() ) return(-1);
        if (cat1.getPoidsMax() < getPoidsMax()) return(1);
        return(0);
    }
    
    
    public int getAnneeMax() {
        return anneeMax;
    }
    
    public int getAnneeMin() {
        return anneeMin;
    }
    
    public void setAnnee(int anneeMin,int anneeMax) {
        this.anneeMin = anneeMin;
        this.anneeMin = anneeMax;
    }
    
    public void enleveParticipant(Participant p) {
        participantsParPoids.remove(p);
        participantsParPoules.remove(p);
        if (p.getCategorie() != null)
            p.setCategorie(null);
    }
    public void addParticipant(Participant p, boolean modifCategorie) {
        participantsParPoids.add(p);
        participantsParPoules.add(p);
        if (modifCategorie) p.setCategorie(this);
    }
    
    public void addParticipants(Participants p, boolean modifCategorie) {
        participantsParPoids.addAll(p);
        participantsParPoules.addAll(p);
        if (modifCategorie) {
            Iterator it = p.iterator();
            while (it.hasNext()) {
                ((Participant) it.next()).setCategorie(this);
            }
        }
    }
    public void setParticipants(Participants p, boolean modifCategorie) {
        participantsParPoids.removeAllElements();
        participantsParPoules.removeAllElements();
        addParticipants(p, modifCategorie);
    }
    public void ordonneParticipants() {
        Collections.sort(participantsParPoids);
        Collections.sort(participantsParPoules,new Comparator() {
            public int compare(Object o1,Object o2) {
                return(((Participant)o1).compareToAvecPoule((Participant)o2));
            }
        });
    }
    public Participants getParticipantsParPoids() {
        return(participantsParPoids);
    }
    public Participants getParticipantsParPoules() {
        return(participantsParPoules);
    }
    
    
    public int nbParticipants() {
        return(participantsParPoids.size());
    }
    
    public Poule getPoule(int noPoule) {
        Iterator it = getParticipantsParPoules().iterator();
        while (it.hasNext()) {
            Participant p = (Participant) it.next();
            if (p.getNoPoule() == noPoule)
                return(p.getPoule());
        }
        return(null);
    }
    public TreeSet getNosPoules() {
        TreeSet nosPoules = new TreeSet();
        Iterator it = getParticipantsParPoules().iterator();
        while (it.hasNext()) {
            Participant p = (Participant) it.next();
            nosPoules.add(p.getNoPoule());
        }
        return(nosPoules);
    }
    public Participant getParticipant(int i) {
        if (i > participantsParPoids.size()) return(null);
        return((Participant)participantsParPoids.get(i));
    }
    public void sauver() {
        getParticipantsParPoids().sauver(Main.getPropriete("Repertoire") + File.separator + getNom() + ".xls");
    }
    
}
