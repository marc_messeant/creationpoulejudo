
/*
 * Tournoi.java
 *
 * Created on 22 octobre 2006, 17:03
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */
package tournoijudo;

//~--- JDK imports ------------------------------------------------------------

import java.io.*;
import jxl.*;
import jxl.write.*;
import java.awt.*;
import java.lang.reflect.Array;
import java.util.*;
import javax.swing.*;
import jxl.*;
import jxl.read.biff.BiffException;
import jxl.read.biff.PasswordException;
//~--- classes ----------------------------------------------------------------

/**
 *
 * @author Marc
 */
public class Tournoi implements Serializable {
    
    public final static int TOUNOI_CREE = 1;
    
    private String nom;
    private Vector clubs = new Vector();
    private TreeSet categories = new TreeSet();
    private transient PanneauTournoi panneau;
    public static  Properties proprietes = new Properties();
    private int etat = 0;
    
    
    public int[] colonnes = new int[Participants.nomPropriete.length];
    public Hashtable noColonne = new Hashtable();
    //~--- constructors -------------------------------------------------------
    
    /** Creates a new instance of Tournoi */
    public Tournoi(String nom) {
        DialogTraces traces = new DialogTraces();
        this.setNom(nom);
        String nomFichier = Main.getPropriete("Repertoire") + File.separator + "Categories.xls";
        if (!(new File(nomFichier)).exists()) {
            nomFichier = Main.getPropriete("Repertoire") + File.separator + ".." + File.separator + "Categories.xls";
        }
        if (!(new File(nomFichier)).exists()) {
            traces.ecrireErreur("il n'y a pas de fichier Catégorie : \n\t" + nomFichier + "\nou dans le repertoire père");
            return;
        }
        lireCategories(nomFichier);
        nomFichier = Main.getPropriete("Repertoire") + File.separator + "TournoiJudo.cfg";
        if (!(new File(nomFichier)).exists()) {
            nomFichier = Main.getPropriete("Repertoire") + File.separator + ".." + File.separator + "TournoiJudo.cfg";
        }
        if (!(new File(nomFichier)).exists()) {
            traces.ecrireErreur("il n'y a pas de fichier TournoiJudo : \n\t" + nomFichier + "\nou dans le repertoire père");
            return;
        }
        try {
            proprietes.loadFromXML(new FileInputStream(nomFichier));
        } catch (FileNotFoundException ex) {
            traces.ecrireErreur(ex.getLocalizedMessage());
        } catch (IOException ex) {
            traces.ecrireErreur(ex.getLocalizedMessage());
        }
        int i;
        for (i=0; i < Participants.nomPropriete.length ; i++) {
            if (proprietes.getProperty(Participants.nomPropriete[i]) == null) {
                traces.ecrireErreur("La propriété : " + Participants.nomPropriete[i] + " n'est pas déclarée adns le fichier de Config");
            } else {
                try {
                    noColonne.put(Participants.nomPropriete[i],Integer.parseInt(proprietes.getProperty(Participants.nomPropriete[i],String.valueOf(i))));
                    colonnes[Integer.parseInt(proprietes.getProperty(Participants.nomPropriete[i],String.valueOf(i)))] = i;
                } catch (java.lang.IndexOutOfBoundsException e) {
                    traces.ecrireErreur("Erreur de chargement pour la propriété " + Participants.nomPropriete[i] + " : indice hors plage = " + e.getLocalizedMessage());
                }catch (Exception e) {
                    traces.ecrireErreur("Erreur de chargement pour la propriété " + Participants.nomPropriete[i] + " : " + e.getLocalizedMessage());
                }
            }
        }
        traces.mettreEnForme();
        etat = TOUNOI_CREE;
    }
    public boolean isOk() {
        return(etat == TOUNOI_CREE);
    }
    public void setPanneau(PanneauTournoi panneau) {
        this.panneau = panneau;
    }
    public boolean lireCategories(String nomFichier) {
        try {
            File fichier = new File(nomFichier);
            fichier.getCanonicalPath();
            Workbook classeur = Workbook.getWorkbook(fichier);
            Sheet feuille = classeur.getSheet(0);
            int nbLignes = feuille.getRows();
            int ligne;
            for (ligne = 1 ; ligne < nbLignes ; ligne ++) {
                int col = 0;
                String nom, sexe,anneeMin,anneeMax,poidsMax,taillePoule,taillePouleMax,tolerancePoids,toleranceClub,nom2;
                nom = feuille.getCell(col++,ligne).getContents();
                sexe = feuille.getCell(col++,ligne).getContents();
                anneeMin = feuille.getCell(col++,ligne).getContents();
                anneeMax = feuille.getCell(col++,ligne).getContents();
                poidsMax = feuille.getCell(col++,ligne).getContents();
                taillePoule = feuille.getCell(col++,ligne).getContents();
                taillePouleMax = feuille.getCell(col++,ligne).getContents();
                tolerancePoids = feuille.getCell(col++,ligne).getContents();
                toleranceClub = feuille.getCell(col++,ligne).getContents();
                nom2 = feuille.getCell(col++,ligne).getContents();
                getCategories().add(new Categorie(
                        nom,
                        sexe,
                        (anneeMin.equals("")?0:Integer.parseInt(anneeMin)),
                        (anneeMax.equals("")?0:Integer.parseInt(anneeMax)),
                        (poidsMax.equals("")?Float.MAX_VALUE:Float.parseFloat(poidsMax)),
                        0,
                        (taillePoule.equals("")?0:Integer.parseInt(taillePoule)),
                        (taillePouleMax.equals("")?0:Integer.parseInt(taillePouleMax)),
                        (tolerancePoids.equals("")?0:Float.parseFloat(tolerancePoids)),
                        (taillePouleMax.equals("")?0:Integer.parseInt(toleranceClub)),
                        nom2));
            }
            // Collections.sort(getClassesAge());
            Iterator it = getCategories().iterator();
            int anneePrecedente = 0;
            float poidsMaxPrecedent = 0;
            String sexePrecedent = "";
            // il faut mettre les poids Min aux cat�gories
            while (it.hasNext()) {
                Categorie categorieCourante = (Categorie) it.next();
                if (anneePrecedente != categorieCourante.getAnneeMin() && !sexePrecedent.equals(categorieCourante.getSexe())) {
                    poidsMaxPrecedent = 0;
                    anneePrecedente= categorieCourante.getAnneeMin();
                }
                categorieCourante.setPoidsMin(poidsMaxPrecedent);
                poidsMaxPrecedent = categorieCourante.getPoidsMax();
            }
            return(true);
        } catch (Exception e) {
            System.out.println(e);
            return(false);
        }
    }
    public Categorie trouveCategorie(String sexe, int annee, float poids) {
        Iterator it = getCategories().iterator();
        while (it.hasNext()) {
            Categorie categorieCourante = (Categorie) it.next();
            if (categorieCourante.getSexe().equals(sexe) && categorieCourante.getAnneeMin() <= annee && categorieCourante.getAnneeMax() >= annee
                    && categorieCourante.getPoidsMin() <= poids && categorieCourante.getPoidsMax() >= poids) {
                return(categorieCourante) ;
            }
        }
        return(null);
    }
    public Categorie trouveCategorie(String nom) {
        Iterator it = getCategories().iterator();
        while (it.hasNext()) {
            Categorie categorieCourante = (Categorie) it.next();
            if (categorieCourante.getNom().equals(nom)) {
                return(categorieCourante) ;
            }
        }
        return(null);
    }
    public String rechercheCategorie(String sexe, int annee, float poids) {
        Categorie c = trouveCategorie(sexe, annee, poids);
        if (c == null) return("");
        return(c.getNom());
    }
    

    public String getNom() {
        return nom;
    }
    
    public void setNom(String nom) {
        this.nom = nom;
    }
    
    
    public TreeSet getCategories() {
        return categories;
    }
    
    public Vector getClubs() {
        return clubs;
    }
    
    public Club getClub(int index) {
        if (index >= clubs.size()) return(null);
        return ((Club)clubs.get(index));
    }
    public int getNbClubs() {
        return(clubs.size());
    }
    public void addClub(Club club) {
        clubs.add(club);
        panneau.mettreClubsAJour();
    }
    public void rechargerClubs() {
        Iterator it = clubs.iterator();
        while (it.hasNext()) {
            ((Club)it.next()).recharger();
        }
    }
    public void mettreAJourClassesAge() {
        Participants tousParticipants = new Participants();
        {
            Iterator it = getClubs().iterator();
            while (it.hasNext()) {
                Club clubCourant = (Club) it.next();
                tousParticipants.addAll(clubCourant.getParticipants());
            }
        }
        Collections.sort(tousParticipants);
        
        Iterator it = categories.iterator();
        while (it.hasNext()) {
            Categorie catCourante = (Categorie) it.next();
            Participants participantsAge = tousParticipants.filtreParticipants(catCourante);
            catCourante.setParticipants(participantsAge,true); // on remet � jour les categories car on a pu changer une ann�e dans les tableaux
        }
        
        
    }
//    public boolean sauver(String nomFichier) {
//        try {
//            FileOutputStream fos = new FileOutputStream(nomFichier);
//            ObjectOutputStream oos = new ObjectOutputStream(fos);
//            oos.writeObject(this);
//            oos.flush();
//            oos.close();
//            return (true);
//        } catch (Exception e) {
//            System.out.println(e);
//            return(false);
//        }
//    }
    public Club trouverClub(String nom) {
        Iterator it = clubs.iterator();
        while (it.hasNext()) {
            Club cl = (Club)it.next();
            if (cl.getNom().equals(nom)) return(cl);
        }
        return(null);
    }
    public void sauver(File fichier) {
        Participants tousLesParticipants = new Participants();;
        Iterator it = getClubs().iterator();
        while (it.hasNext()) {
            tousLesParticipants.addAll(((Club) it.next()).getParticipants());
        }
        tousLesParticipants.sauver(fichier);
    }
    public void sauvegarde() {
        //Transformation de la date exprim�e en milliseconde, en date plus courante
        Date date=new Date();
        //Mise en place d'un calendrier � partir de la date
        Calendar calendrier=Calendar.getInstance();
        calendrier.setTime(date);
        //Renvoie des donn�es
        String dateCompl = calendrier.get(Calendar.YEAR) + "_"
                + calendrier.get(Calendar.MONTH) + "_"
                + calendrier.get(Calendar.DAY_OF_MONTH) + "_"
                + calendrier.get(Calendar.HOUR_OF_DAY) + "_"
                + calendrier.get(Calendar.MINUTE) + "_"
                + calendrier.get(Calendar.SECOND);
        String nom = Main.getPropriete("Repertoire") + File.separator + "Sauvegardes" + File.separator + Main.getPropriete("NomTournoi") ;
        nom = nom + "_" + dateCompl + ".xls";
        this.sauver(new File(nom));
    }
    public void chargerClubs(Workbook classeur) {
        int nbFeuilles = classeur.getSheets().length;
        int feuille;
        int ligne = 0;
        DialogTraces traces = new DialogTraces();
        int colOuiNon = -1;
        if (noColonne.get("OuiNon") != null) {
            colOuiNon = (Integer) noColonne.get("OuiNon");
        }
        for (feuille= 0 ; feuille < nbFeuilles; feuille ++) {
            Sheet feuilleExcel = classeur.getSheet(feuille);
            int nbLignes = feuilleExcel.getRows();
            for (ligne = 1 ; ligne < nbLignes; ligne ++) {
                String ouiNon = "";
                if (colOuiNon == -1) {
                    ouiNon = "O";
                } else {
                    try {
                        ouiNon = feuilleExcel.getCell(colOuiNon,ligne).getContents();
                    } catch (java.lang.ArrayIndexOutOfBoundsException ex) {
                        ouiNon = "";
                    }
                }
                if (!(ouiNon.equalsIgnoreCase("O") || ouiNon.equalsIgnoreCase("N"))) {
                    traces.ecrireErreur("Ligne : " + (ligne+1) + "\tColonne Oui/Non pas ou mal renseign�e mis � 'N'" );
                    ouiNon = "N";
                }
                
                if (ouiNon.equalsIgnoreCase("O")) {
                    boolean erreur = false;
                    String nomClub = feuilleExcel.getCell((Integer) noColonne.get("Club"),ligne).getContents();
                    if (nomClub.equals("")) {
                        traces.ecrireErreur("Ligne : " + (ligne+1) + "\tIl n'y a pas de club" );
                        erreur = true;
                    } else {
                        Club club = trouverClub(nomClub);
                        if (club == null) {
                            club = new Club(nomClub);
                            clubs.add(club);
                        }
                        Participant p = new Participant();
                        int noCol = 0;
                        int nbCols = feuilleExcel.getColumns();
                        if (nbCols > Main.getTournoiCourant().colonnes.length && ligne == 1) {
                            traces.ecrireWarning("Il y a des colonnes non lues � la fin des lignes,\n\t" + nbCols + " colonnes remplies dans la feuille et " + Main.getTournoiCourant().colonnes.length + "d�clar�es.");
                        }
                        for (noCol = 0 ; noCol <  nbCols ; noCol++) {
                            if (noCol < Main.getTournoiCourant().colonnes.length) {
                                try {
                                    String ret = p.setPropriete(Participants.nomPropriete[Main.getTournoiCourant().colonnes[noCol]],feuilleExcel.getCell(noCol,ligne).getContents());
                                    if (!ret.equals("")) {
                                        erreur = true;
                                        traces.ecrireErreur("Cellule(L" + (ligne+1) + ",C" + (noCol+1) + ")\t"
                                                + Participants.nomPropriete[Main.getTournoiCourant().colonnes[noCol]] + "\t" + ret);
                                    }
                                } catch (java.lang.ArrayIndexOutOfBoundsException e) {
                                    traces.ecrireWarning("Ligne : " + (ligne + 1) + " Colonne " + (noCol + 1) + "\t" + e.getLocalizedMessage());
                                } catch (Exception e) {
                                    erreur = true;
                                    traces.ecrireErreur("Cellule(L" + (ligne+1) + ",C" + (noCol+1) + ")\t"
                                            + Participants.nomPropriete[Main.getTournoiCourant().colonnes[noCol]] + "\t" + e.getLocalizedMessage());
                                }
                            }
                        }
                        if (erreur) {
                            p.dispose();
                        } else {
                            String ret = p.ajouteDansClub(club);
                            if (!ret.equals("")) {
                                traces.ecrireWarning("Cellule(L" + (ligne+1) + ",C" + (noCol+1) + ")\t" + ret);
                            }
                            if (p.getClub() == null) { // il n'a pas �t� charg� dans le club
                                p.dispose();
                            }
                        }
                    }
                }
            }
        }
        traces.mettreEnForme();
        return;
    }
    public boolean ecrireResultat() {
        String sqlStart = "INSERT INTO \"Resultat\" (";
        int noColonne = 0;
        int noLigne = 0;
        for (noColonne = 1 ; noColonne < this.getClubs().size() ; noColonne++) {
            sqlStart = sqlStart + this.getClub(noColonne - 1).getNom() + ",";
        }
        sqlStart = sqlStart.substring(0,sqlStart.length() - 2) + ") VALUES (";
        // TODO � Finir si besoin
        return(true);
    }
    public boolean ecrireResultat(File fichier) {
        WritableSheet feuille;
        WritableWorkbook classeur = null;
        try {
            classeur = (WritableWorkbook)Workbook.createWorkbook(fichier);
            feuille = classeur.createSheet("Feuille",0);
            int noColonne = 0;
            int noLigne = 0;
            for (noColonne = 1 ; noColonne < this.getClubs().size() ; noColonne++) {
                jxl.write.Label labelNom = new jxl.write.Label(noColonne,0,this.getClub(noColonne - 1).getNom());
                feuille.addCell(labelNom);
            }
            int noResultat = 0;
            for (noResultat = 0 ; noResultat < 10 ; noResultat ++) {
                boolean premierLigne = true;
                for (noColonne = 1 ; noColonne < this.getClubs().size() ; noColonne++) {
                    double nbResultat = this.getClub(noColonne - 1).getParticipants().filtreParticipants(noResultat).size();
                    if (nbResultat > 0) {
                        if (premierLigne) {
                            noLigne++;
                            jxl.write.Number nombre = new jxl.write.Number(0,noLigne,(double) noResultat);
                            feuille.addCell(nombre);
                            premierLigne = false;
                        }
                        jxl.write.Number nombre = new jxl.write.Number(noColonne,noLigne, nbResultat);
                        feuille.addCell(nombre);
                    }
                }
            }
            
        } catch (Exception e) {
            System.err.println(e);
            return(false);
        }
        try {
            if (classeur != null) {
                classeur.write();
                classeur.close();
            }
        } catch (Exception e) {
            System.err.println(e);
            return(false);
        }
        return(true);
    }
}
