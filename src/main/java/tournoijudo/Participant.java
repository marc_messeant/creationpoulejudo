package tournoijudo;
//~--- JDK imports ------------------------------------------------------------

import java.io.Serializable;

/*
 * Participant.java
 *
 * Created on 22 octobre 2006, 16:42
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */

//~--- classes ----------------------------------------------------------------

/**
 *
 * @author Marc
 */
public class Participant implements Comparable,Serializable {
    private String sexe = "";
    private int anneeNaissance = 0;
    private String grade = "";
    private String nom = "Nouveau";
    private float  poids = 0.0F;
    private String prenom = "";
    private transient Poule poule = null;
    private Club club = null;
    private Categorie categorie = null;
    private int placeDansPoule=0;
    private boolean supprime = false;
    //~--- constructors -------------------------------------------------------
    
    /** Creates a new instance of Participant */
    public Participant(String nom, String prenom, float poids,String sexe, int anneeNaissance, String grade, Club club) {
        this.setNom(nom);
        this.setPrenom(prenom);
        this.setPoids(poids);
        this.setSexe(sexe);
        this.setAnneeNaissance(anneeNaissance);
        this.setGrade(grade);
        this.setClub(club);
        Categorie c =  Main.getTournoiCourant().trouveCategorie(sexe,anneeNaissance,poids);
        if (c!=null) {
            c.addParticipant(this,true);
        }
    }
    public Participant(Club club) {
        this.setClub(club);
    }
    
    public Participant() {
    }
    public String ajouteDansClub(Club club) {
        Participant participantDansClub = club.getParticipant(nom,prenom);
        // Il n'existe pas'
        if (participantDansClub == null) {
            club.ajouteParticipant(this);
            this.setClub(club);
            return("");
        }
        // Je n'avais pas de poule en mémoire on enlève celui en mémoire pour mettre le nouveau
        if (participantDansClub.getNoPoule() == 0) {
            Categorie c = participantDansClub.getCategorie();
            if (c != null) {
                c.enleveParticipant(participantDansClub);
            }
            club.supprimerParticipant(participantDansClub);
            club.ajouteParticipant(this);
            this.setClub(club);
            return("");
        }
        // Il était déjà dans une poule mais différente, on ne le charge pas dans le club
        if (participantDansClub.getNoPoule() != this.getNoPoule())
            return(nom + " " + prenom + " n'est pas ajouté car il est déjà dans une poule");
        // Il était dans la même poule on ne le charge pas dans le club
        return ("");
    }
    
    public String getSexe() {
        return sexe;
    }
    
    public void setSexe(String sexe) {
        this.sexe = sexe;
    }
    
    public int getAnneeNaissance() {
        return anneeNaissance;
    }
    
    public void setAnneeNaissance(int anneeNaissance) {
        this.anneeNaissance = anneeNaissance;
    }
    
    public String getGrade() {
        return grade;
    }
    
    public void setGrade(String grade) {
        this.grade = grade;
    }
    
    public String getNom() {
        return nom;
    }
    
    public void setNom(String nom) {
        this.nom = nom;
    }
    
    public float getPoids() {
        return poids;
    }
    
    public void setPoids(float poids) {
        this.poids = poids;
    }
    
    public String getPrenom() {
        return prenom;
    }
    
    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }
    
    public void setCategorie(Categorie c) {
        this.categorie = c;
    }
    /**
     * renvoie la ctégorie du participant
     * @return la catégorie ou null si pas de catégorie
     */
    public Categorie getCategorie() {
        return(categorie);
    }
    public int compareTo(Object o) {
        Participant p = (Participant)o;
        if (p.getSexe().compareTo(this.getSexe()) != 0) return(p.getSexe().compareTo(this.getSexe()));
        if (p.getPoids() > getPoids() ) return(-1);
        if (p.getPoids() < getPoids()) return(1);
        if (p.getAnneeNaissance() > getAnneeNaissance()) return(-1);
        if (p.getAnneeNaissance() < getAnneeNaissance()) return(1);
        return(0);
    }
    public int compareToAvecPoule(Object o) {
        Participant p = (Participant)o;
        if (p.getNoPoule() > this.getNoPoule()) return(-1);
        if (p.getNoPoule() < this.getNoPoule()) return(1);
        if (p.getSexe().compareTo(this.getSexe()) != 0) return(p.getSexe().compareTo(this.getSexe()));
        if (p.getPoids() > getPoids() ) return(-1);
        if (p.getPoids() < getPoids()) return(1);
        if (p.getAnneeNaissance() > getAnneeNaissance()) return(-1);
        if (p.getAnneeNaissance() < getAnneeNaissance()) return(1);
        return(0);
    }
    
    public Object getPropriete(String nom) {
        if (nom.equals("Nom")) return(getNom());
        if (nom.equals("Prenom")) return(getPrenom());
        if (nom.equals("AnneeNaissance")) return(getAnneeNaissance());
        if (nom.equals("Poids")) return(getPoids());
        if (nom.equals("Sexe")) return(getSexe());
        if (nom.equals("Grade")) return(getGrade());
        if (nom.equals("Categorie")) return((getCategorie() != null?getCategorie().getNom():""));
        if (nom.equals("Poule")) return(getNoPoule());
        if (nom.equals("Resultat")) return(getPlaceDansPoule());
        if (nom.equals("Club")) return(getClub().getNom());
        if (nom.equals("OuiNon")) return("O");
        return("");
    }
    public String setPropriete(String nom,Object val) {
        String retour = "";
        if (nom.equals("Nom"))
            if (val.getClass().getName().equals("java.lang.String"))
                setNom((String)val);
        if (nom.equals("Prenom"))
            if (val.getClass().getName().equals("java.lang.String"))
                setPrenom((String)val);
        if (nom.equals("AnneeNaissance")) {
            int an = 0;
            if (val.getClass().getName().equals("java.lang.String"))
                an = Integer.parseInt((String)val);
            else
                if (val.getClass().getName().equals("java.lang.Integer"))
                    an = (Integer)val;
            if (an < 1930 || an > 2030)
                retour += ("Année Naissance incorrecte : '" + an + "' pour '" + getNom() + "'");
            else
                setAnneeNaissance(an);
        }
        if (nom.equals("Poids")) {
            Float poids = 0.0F;
            if (val.getClass().getName().equals("java.lang.String"))
                poids = Float.parseFloat(((String)val).replace(',','.'));
            else
                if (val.getClass().getName().equals("java.lang.Float"))
                    poids =(Float)val;
            if (poids < 10.0F || poids > 200.0F)
                retour += ("Poids incorrect : '" + poids + "' pour '" + getNom() + "'");
            else
                setPoids(poids);
        }
        if (nom.equals("Sexe")) {
            if (val.getClass().getName().equals("java.lang.String")) {
                String sexe = (String ) val;
                if (sexe.equals("F") || sexe.equals("M"))
                    setSexe(sexe);
                else
                    retour += ("Sexe : '" + sexe + "' incorrect pour '" + getNom() + "'");
            } else
                retour += ("Sexe incorrect pour '" + getNom() + "'");
        }
        if (nom.equals("Grade"))
            if (val.getClass().getName().equals("java.lang.String"))
                setGrade((String)val);
        Categorie cat = null;
        if (nom.equals("Categorie")) {
            if (val.getClass().getName().equals("tournoijudo.Categorie"))
                categorie = (Categorie) val;
            else {
                if (val.getClass().getName().equals("java.lang.String")) {
                    if (!((String)val).equals("")) {
                        cat = Main.getTournoiCourant().trouveCategorie((String) val);
                        if (cat != null) {
                            if (categorie != null && cat != getCategorie()) {
                                categorie.enleveParticipant(this);
                                cat.addParticipant(this,true);
                            }
                        } else
                            retour += ("Categorie : '" + val + "' non trouvé pour '" + getNom() + "'");
                    }
                }
            }
        }
        if (nom.equals("Poule")) {
            if (val.getClass().getName().equals("tournoi.Poule")) {
                setPoule((Poule)val);
            } else {
                int noPoule = 0;
                if (val.getClass().getName().equals("java.lang.Integer"))
                    noPoule = (Integer) val;
                if (val.getClass().getName().equals("java.lang.String")) {
                    if (((String)val).equals("")) {
                        return("");
                    }
                    noPoule = Integer.parseInt((String) val);
                }
                Poule poule = null;
                if (categorie != null) {
                    poule = categorie.getPoule(noPoule);
                    if (poule == null && noPoule != 0) {
                        poule = new Poule(noPoule);
                    }
                }
                if (poule != null)
                    poule.ajouteParticipant(this);
            }
        }
        if (nom.equals("Resultat"))
            if (val.getClass().getName().equals("java.lang.String")) {
            if (((String)val).equals("")) {
                return("");
            }
            setPlaceDansPoule(Integer.parseInt((String)val));
            } else
                if (val.getClass().getName().equals("java.lang.Integer"))
                    setPlaceDansPoule((Integer)val);
        // on regarde si on peut le mettr dans une cat�gorie
        if (getCategorie() == null) {
            if (getAnneeNaissance() != 0 && getPoids() != 0 && !getSexe().equals("")) {
                cat = Main.getTournoiCourant().trouveCategorie(getSexe(),getAnneeNaissance(),getPoids());
                if (cat != null) {
                    if (cat != getCategorie()) {
                        if (categorie != null)
                            categorie.enleveParticipant(this);
                        cat.addParticipant(this,true);
                    }
                }
            }
        }
        return(retour);
    }
    /**
     * renvoie la oule dans laquelle est le participant
     * @return la poule (Poule) ou null si pas de poule
     */
    public Poule getPoule() {
        return poule;
    }
    /**
     * renvoie le numéro de la poule dans laquelle est le participant
     * @return le numéro de la poule ou 0 si pas de poule
     */
    public int getNoPoule() {
        if (poule== null) return(0);
        return poule.noPoule;
    }
    
    public void setPoule(Poule poule) {
        if (this.poule == null)
            this.poule = poule;
        else {
            this.poule.enleveParticipant(this);
            this.poule = poule;
        }
    }
    
    public Club getClub() {
        return club;
    }
    
    public void setClub(Club club) {
        this.club = club;
    }
    
    public int getPlaceDansPoule() {
        return placeDansPoule;
    }
    
    public void setPlaceDansPoule(int placeDansPoule) {
        this.placeDansPoule = placeDansPoule;
    }
    /**
     * indique si ce participant a été supprimé
     * @return vra�i si le participant a été supprimé
     */
    public boolean isSupprime() {
        return(supprime);
    }
    public void dispose() {
        if (this.club != null)
            this.club.supprimerParticipant(this);
        this.club=null;
        if (this.categorie != null)
            this.categorie.enleveParticipant(this);
        this.categorie = null;
        if (this.poule != null)
            this.poule.enleveParticipant(this);
        this.poule=null;
        this.supprime = true;
    }
}



//~ Formatted by Jindent --- http://www.jindent.com
