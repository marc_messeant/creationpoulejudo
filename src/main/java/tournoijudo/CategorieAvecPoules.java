/*
 * CategorieAvecPoules.java
 *
 * Created on 20 novembre 2006, 12:33
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */

package tournoijudo;
import java.util.*;
import java.io.*;

/**
 *
 * @author Marc
 */
public class CategorieAvecPoules extends Categorie {
    private transient Vector poules = null;
    public boolean priseEnCompteCategorie = true;
    
    /** Creates a new instance of CategorieAvecPoules */
    public CategorieAvecPoules(String nom,String sexe,int anneeMin,int anneeMax, int taillePoule, int taillePouleMax, float tolerancePoids, int toleranceClub) {
        super( nom, sexe,anneeMin,anneeMax,Float.MAX_VALUE,0, taillePoule, taillePouleMax, tolerancePoids, toleranceClub,nom);
    }
    
    private Poule creerPoule() {
        int no = poules.size()+1;
        while(true) {
            try {
                if (poules.get(no) != null) {
                    Poule poule= new Poule(no);
                    poules.add(poule);
                    return(poule);
                } else no++;
            } catch(ArrayIndexOutOfBoundsException e) {
                Poule poule= new Poule(no);
                poules.add(poule);
                return(poule);
            }
        }
    }
    public void renumeroterPoules() {
        Vector poules = this.getPoules();
        int no;
        for (no=1;no<poules.size();no++) {
            Poule p = (Poule)poules.get(no);
            if ((p == null) || (p.nbParticipants() == 0))
                poules.remove(no--);
        }
        no = 1;
        Iterator it = poules.iterator();
        while (it.hasNext()) {
            Poule poule = (Poule)it.next();
            if (poule != null) poule.noPoule = no++;
        }
    }
    
    public Poule creerPoule(int no) {
        if (no > poules.size()) {
            Poule poule= new Poule(no);
            poules.add(poule);
            return(poule);
        }
        return(null);
    }
    public Poule getPoule(int no) {
        if (poules == null)
            return(null);
        Iterator it = getPoules().iterator();
        while (it.hasNext()) {
            Poule poule = (Poule) it.next();
            if ((poule != null) && (poule.noPoule == no) )
                return(poule);
        }
        return(null);
    }
    public Vector getPoules() {
        return(poules);
    }
    public Vector getPoules(int taille) {
        Vector retour = new Vector();
        if (poules == null)
            return(retour);
        Iterator it = getPoules().iterator();
        while (it.hasNext()) {
            Poule poule = (Poule) it.next();
            if ((poule != null) && (poule.getParticipants().size() == taille) )
                retour.add(poule);
        }
        return(retour);
    }
    public void miseAJourPoules() {
        if (poules == null)
            poules = new Vector();
        else
            poules.removeAllElements();
        Iterator  it = getParticipantsParPoids().iterator();
        while (it.hasNext()) {
            Participant participantCourant = (Participant) it.next();
            if (participantCourant.getPoule() != null) {
                if (!poules.contains(participantCourant.getPoule())) {
                    poules.add(participantCourant.getPoule());
                }
            }
        }
        ordonneParticipants();
    }
    private void completerPoule(Poule poule,int noParticipant) {
        completerPoule(poule,noParticipant,1);
    }
    private void completerPoule(Poule poule,int noParticipant,int noPassage) {
        Iterator  it = getParticipantsParPoids().listIterator(noParticipant);
        while (it.hasNext()) {
            Participant participantCourant = (Participant) it.next();
            if (poule.getCategoriePremier() != participantCourant.getCategorie() && priseEnCompteCategorie)
                return;
            float ecartPoids = poule.ecartPoids(participantCourant);
            if (participantCourant.getPoule() == null) {
                int dejaClub = poule.nbDejaClub(participantCourant.getClub());
                switch (noPassage) {
                    case 1: // on regarde si les deux critères sont vérifiés
                        if (ecartPoids < this.tolerancePoids && dejaClub < this.toleranceClub) {
                            poule.ajouteParticipant(participantCourant);
                        }
                        break;
                    case 2: // ensuite que le poids
                        if (ecartPoids < this.tolerancePoids) {
                            poule.ajouteParticipant(participantCourant);
                        }
                        
                        break;
                    case 3: // ensuite que le club si pas exagéré pour le poids (deux fois la tol�rance)
                        if (ecartPoids < (this.tolerancePoids * 2) && dejaClub <= this.toleranceClub) {
                            poule.ajouteParticipant(participantCourant);
                        }
                        break;
                }
            }
            if (poule.nbParticipants() >= this.taillePoule) {
                if (poule.nbParticipants() >= this.taillePouleMax) { // si premier passage on peut quand même continuer à remplir si pas max atteind
                    break;
                }
                if (noPassage > 1) {
                    break;
                }
                if (!encorePouleSup(noParticipant + poule.nbParticipants())) {
                    break;
                }
            }
            if (ecartPoids > (this.tolerancePoids * 4))
                break; // quand on est à 4 fois la tolérance de poids on abandonne
        }
        if (noPassage < 3 && poule.nbParticipants() < this.taillePouleMax )
            completerPoule(poule,noParticipant,noPassage + 1);
        else
            return;
    }
    private boolean encorePouleSup(int nbParticipantsDejaAffectes) {
        int nbParticipants = getParticipantsParPoids().size();
        int reste = (nbParticipants - nbParticipantsDejaAffectes ) % this.taillePoule;
        return(reste >0 && reste < this.taillePoule );
    }
    public void completerPoules() {
        int nbParticipants = getParticipantsParPoids().size();
        int reste = nbParticipants % this.taillePoule;
        Poule pouleCourante = creerPoule();
        int nbDansPoule = 0;
        boolean nouvellePoule = false;
        int noParticipantCourant = 0;
        Iterator  it = getParticipantsParPoids().iterator();
        while (it.hasNext()) {
            Participant participantCourant = (Participant) it.next();
            noParticipantCourant++;
            if (participantCourant.getPoule() == null) {
                pouleCourante = creerPoule();
                pouleCourante.ajouteParticipant(participantCourant);
                completerPoule(pouleCourante,noParticipantCourant);
            }
        }
        return;
        
    }
    public void creerPoules() {
        Iterator  it = getParticipantsParPoids().iterator();
        while (it.hasNext()) {
            Participant participantCourant = (Participant) it.next();
            participantCourant.setPoule(null);
        }
        if (poules == null) poules = new Vector();
        poules.removeAllElements();
        completerPoules();
        ordonneParticipants();
    }
    public int tailleMaxPoules() {
        Iterator it = poules.iterator();
        int tailleMax = 0;
        while (it.hasNext()) {
            int taillePoule = ((Poule) it.next()).nbParticipants();
            if (tailleMax <= taillePoule)
                tailleMax = taillePoule;
        }
        return(tailleMax);
    }
}
