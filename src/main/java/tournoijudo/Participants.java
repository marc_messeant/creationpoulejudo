/*
 * Participants.java
 *
 * Created on 27 octobre 2006, 14:31
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */

package tournoijudo;
import java.util.*;
import java.io.*;
import jxl.*;
import jxl.write.*;
/**
 *
 * @author Marc
 */
public class Participants extends Vector{
    public final static String[] nomPropriete = {"Sexe","Nom","Prenom","Poids","AnneeNaissance","Grade","Categorie","Poule","Resultat","Club","OuiNon"};
    public final static String[] nomColonnes = {"Sexe","Nom","Prénom","Poids","Année Naissance","Grade","Catégorie","Poule","Résultat","Club","OuiNon"};
    
    /** Creates a new instance of Participants */
    public Participants() {
        super();
    }
    
    public Participants filtreParticipants(Categorie cat) {
        Participants participantsFiltres = new Participants();
        Iterator it = this.iterator();
        while (it.hasNext()) {
            Participant p = (Participant) it.next();
            if (p.getSexe().equals(cat.getSexe()) && p.getAnneeNaissance()<= cat.getAnneeMax() && p.getAnneeNaissance() >= cat.getAnneeMin()) {
                if (p.getPoids() > cat.getPoidsMin() && p.getPoids() <= cat.getPoidsMax())
                    participantsFiltres.add(p);
            }
        }
        return(participantsFiltres);
    }
    public Participants filtreParticipants(int placeDansPoule) {
        Participants participantsFiltres = new Participants();
        Iterator it = this.iterator();
        while (it.hasNext()) {
            Participant p = (Participant) it.next();
            if (p.getPlaceDansPoule() == placeDansPoule) {
                participantsFiltres.add(p);
            }
        }
        return(participantsFiltres);
    }
    public Participant getParticipant(int i) {
        if (i > size()) return(null);
        return((Participant)get(i));
    }

    WritableSheet feuille;
    WritableWorkbook classeur;
    public void sauver(String nomFichier) {
        sauver(new File(nomFichier));
    }
    public void sauver(File fichier) {
        try {
            classeur = (WritableWorkbook)Workbook.createWorkbook(fichier);
            feuille = classeur.createSheet("Feuille",0);
            int noColonne = 0;
            int noLigne = 0;
            for (noColonne = 0 ; noColonne < Participants.nomColonnes.length ; noColonne++)
                insereCell(Participants.nomColonnes[Main.getTournoiCourant().colonnes[noColonne]],noLigne,noColonne,feuille);
            Iterator itParticipant = this.iterator();
            noLigne++;
            while (itParticipant.hasNext()) {
                noColonne = 0;
                Participant participant = (Participant) itParticipant.next();
                for (noColonne = 0 ; noColonne < Participants.nomColonnes.length ; noColonne++)
                    insereCell(participant.getPropriete(Participants.nomPropriete[Main.getTournoiCourant().colonnes[noColonne]]),noLigne,noColonne,feuille);
                noLigne++;
            }
            
        } catch (Exception e) {
            System.err.println(e);
        }
        try {
            classeur.write();
            classeur.close();
        } catch (Exception e) {
            System.err.println(e);
        }
    }
    private void insereCell(Object val,int r, int c,WritableSheet feuille)    {
        try {
            if (val.getClass().getName().equals("java.lang.String")) {
                jxl.write.Label labelNom = new jxl.write.Label(c,r,(String) val);
                feuille.addCell(labelNom);
            } else  if (val.getClass().getName().equals("java.lang.Float")) {
                float valFloat = (Float) val;
                double valDouble = (double) valFloat;
                jxl.write.Number nombre = new jxl.write.Number(c,r,valDouble);
                feuille.addCell(nombre);
            } else  if (val.getClass().getName().equals("java.lang.Integer")) {
                int valInt = (Integer) val;
                double valDouble = (double) valInt;
                jxl.write.Number nombre = new jxl.write.Number(c,r,valDouble);
                feuille.addCell(nombre);
            }
        } catch (jxl.write.WriteException e) {
            System.out.println(e);
        }
    }
    
}
