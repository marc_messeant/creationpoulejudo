package tournoijudo;
//~--- JDK imports ------------------------------------------------------------

import java.io.*;

import java.util.*;
import jxl.*;

//~--- classes ----------------------------------------------------------------

/*
 * Club.java
 *
 * Created on 22 octobre 2006, 16:35
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */

/**
 *
 * @author Marc
 */
public class Club implements Serializable {
    private Vector fichiers = new Vector();
    private Participants participants = new Participants();
    private String nom;
    
    //~--- constructors -------------------------------------------------------
    
    /**
     * Creates a new instance of Club
     * @param nom nom du club à créer
     */
    public Club(String nom) {
        this.nom = nom;
    }
    
    public void vider() {
        while (participants.size() > 0) {
            Participant p = (Participant)participants.get(0);
            Poule poule= p.getPoule();
            if (poule != null) {
                poule.enleveParticipant(p);
            }
            participants.remove(0);
        }
    }
    //~--- methods ------------------------------------------------------------
    public String getNom() {
        return(nom);
    }
    public void setNom(String nom) {
        this.nom = nom;
    }
    public Participants getParticipants() {
        return(participants);
    }
    public Participant getParticipant(int index) {
        return((Participant)participants.get(index));
    }
    /**
     * Renvoie le particpant de ce club ayant le même nom et le même prénom.
     * s'il n'y en a pas , renvoie Null
     * @return Null si pas de particpant de même nom et même prénom
     * @param nom Nom du participant à chercher
     * @param prenom Prénom du participant à chercher
     */
    public Participant getParticipant(String nom, String prenom) {
        Iterator it = getParticipants().iterator();
        Participant p;
        while (it.hasNext()) {
            p = (Participant) it.next();
            if (p.getNom().equals(nom) && p.getPrenom().equals(prenom)) {
                return(p);
            }
        }
        return(null);
    }
    public int getNbParticipants() {
        return(participants.size());
    }
    /**
     * enlève un participant de ce club
     * @param p le participant à enlever
     */
    public void supprimerParticipant(Participant p) {
        getParticipants().remove(p);
    }
    public void recharger() {
        if (fichiers.size() == 0) {
            Main.showMessage("Il n'y a pas de fichiers associés aux clubs chargés");
            return;
        }
        participants.removeAllElements();
        Iterator it = fichiers.iterator();
        while(it.hasNext()) {
            String nomFichier = (String) it.next();
            if (!charger(nomFichier))
                System.err.println("Le fichier : " + nomFichier + " n'est pas chargé\n");;
        }
    }
    public boolean charger(String nomFichier) {
        try {
            Workbook classeur = Workbook.getWorkbook(new File(nomFichier));
            boolean retour = charger(classeur);
            return(retour);
        } catch (Exception e) {
            System.out.println(e);
            return(false);
        }
    }
    public void ajouteFichier(String nomFichier) {
        if (!fichiers.contains(nomFichier)) {
            fichiers.add(nomFichier);
        }
    }
    public boolean charger(Workbook classeur) {
        int nbFeuilles = classeur.getSheets().length;
        int feuille;
        int ligne = 0;
        try {
            for (feuille= 0 ; feuille < nbFeuilles; feuille ++) {
                Sheet feuilleExcel = classeur.getSheet(feuille);
                int nbLignes = feuilleExcel.getRows();
                for (ligne = 1 ; ligne < nbLignes; ligne ++) {
                    participants.add(new Participant(
                            feuilleExcel.getCell(Integer.parseInt(Tournoi.proprietes.getProperty("Nom","1")),ligne).getContents(),
                            feuilleExcel.getCell(Integer.parseInt(Tournoi.proprietes.getProperty("Prenom","2")),ligne).getContents(),
                            Float.parseFloat(feuilleExcel.getCell(Integer.parseInt(Tournoi.proprietes.getProperty("Poids","5")),ligne).getContents().replace(',','.')),
                            feuilleExcel.getCell(Integer.parseInt(Tournoi.proprietes.getProperty("Sexe","0")),ligne).getContents(),
                            Integer.parseInt(feuilleExcel.getCell(Integer.parseInt(Tournoi.proprietes.getProperty("AnneeNaissance","3")),ligne).getContents()),
                            feuilleExcel.getCell(Integer.parseInt(Tournoi.proprietes.getProperty("Grade","4")),ligne).getContents(),
                            this));
                }
            }
            return(true);
        } catch (Exception e) {
            System.out.println("ligne : " + ligne + "\n" + e);
            return(false);
        }
    }
    public Participant creerParticipant(String nom, String prenom, float poids,  String sexe, int anneeNaissance, String grade) {
        Participant p = new Participant(nom, prenom, poids,sexe,anneeNaissance,grade,this);
        participants.add(p);
        
        return (p);
    }
    /**
     * Ajoute un particpant dans la liste des particpants sans vérification
     * @param p Participant à ajouter
     */
    public void ajouteParticipant(Participant p) {
        participants.add(p);
    }
    public Participant creerParticipant() {
        Participant p = new Participant(this);
        participants.add(p);
        
        return (p);
    }
    public String toString() {
        return(nom);
    }
    public void sauver(File fichier) {
        getParticipants().sauver(fichier);
    }
}

//~ Formatted by Jindent --- http://www.jindent.com
