/*
 * PanneauTournoi.java
 *
 * Created on 24 octobre 2006, 18:50
 */

package tournoijudo;
import java.util.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.table.*;

/**
 *
 * @author  Marc
 */
public class PanneauTournoi extends javax.swing.JPanel {
    private Club clubCourant = null;
    private Tournoi tournoiCourant = null;
    
    /** Creates new form PanneauTournoi */
    public PanneauTournoi(Tournoi tournoi) {
        tournoiCourant = tournoi;
        tournoi.setPanneau(this);
        initComponents();
        jListClubs.setListData(tournoiCourant.getClubs());
        if (tournoiCourant.getClubs().size() > 0) {
            jListClubs.setSelectedIndex(0);
            clubCourant = tournoiCourant.getClub(0);
        }
        mettrejTableParticipantsAJour();
        Iterator it = tournoiCourant.getCategories().iterator();
        while (it.hasNext()) {
            jComboBoxCategorie.addItem((Categorie)it.next());
        }
        jSplitPane1.setDividerLocation(0.3);
        jTableParticipants.addMouseListener(new MouseAdapterPerso());
        this.setVisible(true);
    }
    
    public void mettreClubsAJour() {
        int sel = jListClubs.getSelectedIndex();
        jListClubs.setListData(tournoiCourant.getClubs());
        if (sel != -1 && sel < tournoiCourant.getNbClubs()) {
            jListClubs.setSelectedIndex(sel);
            mettrejTableParticipantsAJour();
        }
        mettreCategoriesAJour();
    }
    public void mettreCategoriesAJour() {
        jComboBoxCategorie.removeAllItems();
        Iterator it = tournoiCourant.getCategories().iterator();
        while (it.hasNext()) {
            Categorie c = (Categorie) it.next();
            if (c.nbParticipants() > 0)
                jComboBoxCategorie.addItem(c);
        }
    }
    
    public void mettrejTableParticipantsAJour() {
        jTableParticipants.setModel(
                new AbstractTableModel() {
            public String getColumnName(int col) {
                switch (col) {
                    case 0 :
                        return("Sexe");
                    case 1 :
                        return("Nom");
                    case 2 :
                        return("Prénom");
                    case 3 :
                        return("Naissance");
                    case 4 :
                        return("Grade");
                    case 5 :
                        return("Poids");
                    case 6 :
                        return("Résultat");
                    default:
                        return("Autre");
                }
            }
            public int getRowCount() {
                if (clubCourant == null)
                    return(0);
                return (clubCourant.getNbParticipants()); }
            public int getColumnCount() {
                return (7) ;
            }
            public Object getValueAt(int row, int col) {
                
                if (clubCourant == null) return("");
                switch (col) {
                    case 0 :
                        return(clubCourant.getParticipant(row).getSexe());
                    case 1 :
                        return(clubCourant.getParticipant(row).getNom());
                    case 2 :
                        return(clubCourant.getParticipant(row).getPrenom());
                    case 3 :
                        return(new Integer(clubCourant.getParticipant(row).getAnneeNaissance()));
                    case 4 :
                        return(clubCourant.getParticipant(row).getGrade());
                    case 5 :
                        return(new Float(clubCourant.getParticipant(row).getPoids()));
                    case 6 :
                        return(new Integer(clubCourant.getParticipant(row).getPlaceDansPoule()));
                    default:
                        return("");
                }
            }
            public boolean isCellEditable(int row, int col) {
                if (col != 6 )
                    return true;
                else
                    return false;}
            public void setValueAt(Object value, int row, int col) {
                if (clubCourant == null) return;
                switch (col) {
                    case 0 :
                        clubCourant.getParticipant(row).setSexe((String)value);
                        break;
                    case 1 :
                        clubCourant.getParticipant(row).setNom((String)value);
                        break;
                    case 2 :
                        clubCourant.getParticipant(row).setPrenom((String)value);
                        break;
                    case 3 :
                        clubCourant.getParticipant(row).setAnneeNaissance((Integer)value);
                        break;
                    case 4 :
                        clubCourant.getParticipant(row).setGrade((String)value);
                        break;
                    case 5 :
                        clubCourant.getParticipant(row).setPoids((Float)value);
                        break;
                    default :
                        break;
                }
                fireTableCellUpdated(row, col);
            }
        }
        );
        
    }
    /** This method is called from within the constructor to
     * initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is
     * always regenerated by the Form Editor.
     */
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPopupMenu = new javax.swing.JPopupMenu();
        jMenuItemSupprimer = new javax.swing.JMenuItem();
        jPanelCentre = new javax.swing.JPanel();
        jSplitPane1 = new javax.swing.JSplitPane();
        jScrollPane1 = new javax.swing.JScrollPane();
        jListClubs = new javax.swing.JList();
        jScrollPane2 = new javax.swing.JScrollPane();
        jTableParticipants = new javax.swing.JTable();
        jPanelBoutons = new javax.swing.JPanel();
        jButtonFairePoule = new javax.swing.JButton();
        jButtonChargerClub = new javax.swing.JButton();
        jComboBoxCategorie = new javax.swing.JComboBox();
        jButtonSupprimerClub = new javax.swing.JButton();
        jButtonVoirPoule = new javax.swing.JButton();

        jMenuItemSupprimer.setText("Supprimer");
        jMenuItemSupprimer.setToolTipText("Suppression d'un participant");
        jMenuItemSupprimer.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItemSupprimerActionPerformed(evt);
            }
        });
        jPopupMenu.add(jMenuItemSupprimer);

        setBackground(new java.awt.Color(204, 204, 255));
        setLayout(new java.awt.BorderLayout());

        jPanelCentre.setBackground(new java.awt.Color(51, 255, 204));
        jPanelCentre.setLayout(new java.awt.BorderLayout());

        jSplitPane1.setBackground(new java.awt.Color(204, 204, 255));

        jListClubs.setBackground(new java.awt.Color(255, 255, 153));
        jListClubs.setModel(new javax.swing.AbstractListModel() {
            String[] strings = { "Item 1", "Item 2", "Item 3", "Item 4", "Item 5" };
            public int getSize() { return strings.length; }
            public Object getElementAt(int i) { return strings[i]; }
        });
        jListClubs.addListSelectionListener(new javax.swing.event.ListSelectionListener() {
            public void valueChanged(javax.swing.event.ListSelectionEvent evt) {
                jListClubsValueChanged(evt);
            }
        });
        jScrollPane1.setViewportView(jListClubs);

        jSplitPane1.setLeftComponent(jScrollPane1);

        jTableParticipants.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {

            }
        ));
        jTableParticipants.setAutoResizeMode(javax.swing.JTable.AUTO_RESIZE_ALL_COLUMNS);
        jTableParticipants.setVerifyInputWhenFocusTarget(false);
        jScrollPane2.setViewportView(jTableParticipants);

        jSplitPane1.setRightComponent(jScrollPane2);

        jPanelCentre.add(jSplitPane1, java.awt.BorderLayout.CENTER);

        jButtonFairePoule.setText("Faire Poule");
        jButtonFairePoule.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonFairePouleActionPerformed(evt);
            }
        });

        jButtonChargerClub.setText("Charger Club");
        jButtonChargerClub.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonChargerClubActionPerformed(evt);
            }
        });

        jComboBoxCategorie.setMaximumRowCount(20);

        jButtonSupprimerClub.setText("Supprimer");
        jButtonSupprimerClub.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonSupprimerClubActionPerformed(evt);
            }
        });

        jButtonVoirPoule.setText("Voir Poules");
        jButtonVoirPoule.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonVoirPouleActionPerformed(evt);
            }
        });

        org.jdesktop.layout.GroupLayout jPanelBoutonsLayout = new org.jdesktop.layout.GroupLayout(jPanelBoutons);
        jPanelBoutons.setLayout(jPanelBoutonsLayout);
        jPanelBoutonsLayout.setHorizontalGroup(
            jPanelBoutonsLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(jPanelBoutonsLayout.createSequentialGroup()
                .add(jButtonChargerClub)
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(jButtonSupprimerClub)
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .add(jComboBoxCategorie, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 145, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(jButtonFairePoule))
            .add(org.jdesktop.layout.GroupLayout.TRAILING, jPanelBoutonsLayout.createSequentialGroup()
                .addContainerGap()
                .add(jButtonVoirPoule))
        );
        jPanelBoutonsLayout.setVerticalGroup(
            jPanelBoutonsLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(jPanelBoutonsLayout.createSequentialGroup()
                .add(jPanelBoutonsLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
                    .add(jButtonChargerClub)
                    .add(jButtonFairePoule)
                    .add(jComboBoxCategorie, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                    .add(jButtonSupprimerClub))
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(jButtonVoirPoule)
                .addContainerGap(org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        jPanelCentre.add(jPanelBoutons, java.awt.BorderLayout.SOUTH);

        add(jPanelCentre, java.awt.BorderLayout.CENTER);
    }// </editor-fold>//GEN-END:initComponents
    
    private void jMenuItemSupprimerActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItemSupprimerActionPerformed
// TODO add your handling code here:
        int ligne  = jTableParticipants.getSelectedRow();
        if (ligne > -1) {
            Participant p = clubCourant.getParticipant(ligne);
            p.dispose();
            jTableParticipants.repaint();
        }
    }//GEN-LAST:event_jMenuItemSupprimerActionPerformed
    
    /**
     * fabrique les catégories uniquement basées sur le sexe et l'année
     * @param catSel catégorie sui sera étendue, on se basera sur ses critères de sexe et d'année
     * @return une nouvelle catégorie contenant la catégorie "catSel" et les autres participants de même sexe et de même année
     */
    private CategorieAvecPoules faireCategorie(Categorie catSel) {
        CategorieAvecPoules categorie = null;
        Iterator it = Main.getTournoiCourant().getCategories().iterator();
        while (it.hasNext()) {
            Categorie cat = (Categorie)it.next();
            if (cat.getSexe().equals(catSel.getSexe()) && cat.getAnneeMax() == catSel.getAnneeMax() && cat.getAnneeMin() == catSel.getAnneeMin()) {
                if (categorie == null) {
                    categorie = new CategorieAvecPoules(cat.getNom2(),cat.getSexe(),cat.getAnneeMin(),cat.getAnneeMax(),cat.taillePoule,cat.taillePouleMax,cat.tolerancePoids,cat.toleranceClub);
                }
                categorie.addParticipants(cat.getParticipantsParPoids(),false);
            }
        }
        return(categorie);
    }
    private void jButtonVoirPouleActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonVoirPouleActionPerformed
// TODO add your handling code here:
        Categorie catSel = (Categorie)jComboBoxCategorie.getSelectedItem();
        CategorieAvecPoules categorie = faireCategorie(catSel);
        FramePoules framePoules = new FramePoules(categorie,false);
        framePoules.setVisible(true);
        
    }//GEN-LAST:event_jButtonVoirPouleActionPerformed
    
    private void jButtonSupprimerClubActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonSupprimerClubActionPerformed
// TODO add your handling code here:
        if (jListClubs.getSelectedIndex() != -1) {
            Club clubSel = tournoiCourant.getClub(jListClubs.getSelectedIndex());
            clubSel.vider();
            tournoiCourant.getClubs().remove(clubSel);
        }
        mettreClubsAJour();
    }//GEN-LAST:event_jButtonSupprimerClubActionPerformed
    
    private void jButtonFairePouleActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonFairePouleActionPerformed
// TODO add your handling code here:
        tournoiCourant.mettreAJourClassesAge();
        Categorie catSel = (Categorie)jComboBoxCategorie.getSelectedItem();
        CategorieAvecPoules categorie = faireCategorie(catSel);
        FramePoules framePoules = new FramePoules(categorie,true);
        framePoules.setVisible(true);
    }//GEN-LAST:event_jButtonFairePouleActionPerformed
    
    private void jButtonChargerClubActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonChargerClubActionPerformed
// TODO add your handling code here:
        if (jListClubs.getSelectedIndex() != -1) {
            Club clubSel = tournoiCourant.getClub(jListClubs.getSelectedIndex());
            FrameClub frameClub = new FrameClub(clubSel);
            frameClub.setVisible(true);
        }
    }//GEN-LAST:event_jButtonChargerClubActionPerformed
    
    private void jListClubsValueChanged(javax.swing.event.ListSelectionEvent evt) {//GEN-FIRST:event_jListClubsValueChanged
// TODO add your handling code here:
        if (jListClubs.getSelectedIndex() == -1) {
            clubCourant = null;
        } else {
            clubCourant = Main.getTournoiCourant().getClub(jListClubs.getSelectedIndex());
        }
        mettrejTableParticipantsAJour();
        if (getParent() != null)
            getParent().repaint();
    }//GEN-LAST:event_jListClubsValueChanged
    public class MouseAdapterPerso extends MouseAdapter {
        MouseAdapterPerso() {
        }
        public void mousePressed(MouseEvent e) {
            if (e.isPopupTrigger() || e.isMetaDown()) {
                // This tests for a right-click
                int x = e.getX();  // X-coord of mouse click
                int y = e.getY();  // Y-coord of mouse click
                jPopupMenu.show( jTableParticipants.getComponentAt(x,y), x, y );
            }
        }
    }
    
    
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton jButtonChargerClub;
    private javax.swing.JButton jButtonFairePoule;
    private javax.swing.JButton jButtonSupprimerClub;
    private javax.swing.JButton jButtonVoirPoule;
    private javax.swing.JComboBox jComboBoxCategorie;
    private javax.swing.JList jListClubs;
    private javax.swing.JMenuItem jMenuItemSupprimer;
    private javax.swing.JPanel jPanelBoutons;
    private javax.swing.JPanel jPanelCentre;
    private javax.swing.JPopupMenu jPopupMenu;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JSplitPane jSplitPane1;
    private javax.swing.JTable jTableParticipants;
    // End of variables declaration//GEN-END:variables
    
}
