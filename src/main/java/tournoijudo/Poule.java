/*
 * Poule.java
 *
 * Created on 27 octobre 2006, 11:50
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */

package tournoijudo;
import java.util.*;
import java.io.*;
import jxl.*;
import jxl.write.*;
/**
 *
 * @author Marc
 */
public class Poule {
    
    Participants  participants = new Participants();
    int noPoule;
    /**
     * Creates a new instance of Poule
     * @param noPoule creation d'un poule avec ce numéro.
     */
    public Poule(int noPoule) {
        this.noPoule = noPoule;
        
    }
    
    /**
     * Ajoute le participant à cette poule et positionne la poule de celui-ci.
     * ne le supprime pas de l'ancienne poule
     * @param p participant à ajouter à la poule
     */
    public void ajouteParticipant(Participant p) {
        participants.add(p);
        p.setPoule(this);
    }
    /**
     * enlève le participant de cette poule
     * @param p participant à enlever de cette poule
     */
    public void enleveParticipant(Participant p) {
        if (participants.contains(p)) {
            participants.remove(p);
            p.setPoule(null);
        }
    }
    
    /**
     * nombre de participants
     * @return le nombre de participants de la poule
     */
    public int nbParticipants() {
        return(participants.size());
    }
    
    public Participants getParticipants() {
        return(participants);
    }
    public Participant getParticipant(int index) {
        return((Participant)participants.get(index));
    }

    /**
     * moyenne des poids
     * @return moyenne des poids des participants de la poule
     */
    public float moyennePoids() {
        float s=0;
        if (participants.size() == 0) return(0.0F);
        Iterator  it = participants.iterator();
        while (it.hasNext()) {
            Participant participantCourant = (Participant) it.next();
            s += participantCourant.getPoids();
        }
        return(s / participants.size());
    }
    /**
     * nombre de participants d'un club
     * @param club club dont il faut calculer les participants de cette poule
     * @return nombre de participant de la poule de ce club
     */
    public int nbDejaClub(Club club) {
        int nb=0;
        Iterator  it = participants.iterator();
        while (it.hasNext()) {
            Participant participantCourant = (Participant) it.next();
            if (participantCourant.getClub() == club)
                nb++;
        }
        return(nb);
    }
    /**
     * indicateur d'écart de poids entre le minimum et le maximum.
     * @return Poids minimum en pourcentage du poidsMax
     */
    public float ecartPoids() {
        Iterator it = participants.iterator();
        float poidsMin = Float.MAX_VALUE ,poidsMax=0.0F;
        while (it.hasNext()) {
            Participant p = (Participant)it.next();
            if (p.getPoids() < poidsMin) poidsMin = p.getPoids();
            if (p.getPoids() > poidsMax) poidsMax = p.getPoids();
        }
        return((poidsMax - poidsMin) / poidsMax);
    }
    /**
     * indicateur d'écart de poids entre le minimum et le maximum avce le participant fourni
     * @return Poids minimum en pourcentage du poidsMax
     */
    public float ecartPoids(Participant participant) {
        Iterator it = participants.iterator();
        float poidsMin = Float.MAX_VALUE ,poidsMax=0.0F;
        while (it.hasNext()) {
            Participant p = (Participant)it.next();
            if (p.getPoids() < poidsMin) poidsMin = p.getPoids();
            if (p.getPoids() > poidsMax) poidsMax = p.getPoids();
        }
        if (participant.getPoids() < poidsMin) poidsMin = participant.getPoids();
        if (participant.getPoids() > poidsMax) poidsMax = participant.getPoids();
        return((poidsMax - poidsMin) / poidsMax);
    }
    
    /**
     * nombre de participants de même club au maximum.
     * @return nb maximal de participants de même club
     */
    private int nbClubMax() {
        int nb = 0;
        Iterator it = participants.iterator();
        while (it.hasNext()) {
            Participant p = (Participant)it.next();
            if (nbDejaClub(p.getClub()) > nb) nb = nbDejaClub(p.getClub());
        }
        return(nb);
    }
    public final static int POULE_PB_POIDS = 1;
    public final static int POULE_PB_CLUB = 2;
    public final static int POULE_TAILLE_MAX = 4;
    public final static int POULE_TAILLE_MIN = 8;
    /**
     * avertissements :
     * @return rend un avertissement
     */
    public int getAvertissements(Categorie classeAge) {
        int retour = 0;
        if (ecartPoids() > classeAge.tolerancePoids) retour |= POULE_PB_POIDS;
        if (nbClubMax() > classeAge.toleranceClub) retour |= POULE_PB_CLUB;
        if (nbParticipants() > classeAge.taillePoule) retour |= POULE_TAILLE_MAX;
        if (nbParticipants() < classeAge.taillePoule) retour |= POULE_TAILLE_MIN;
        
        return(retour);
    }
    public Categorie getCategoriePremier() {
        if (participants.size() > 0)
            return(((Participant) participants.get(0)).getCategorie());
        else
            return(null);
    }
}
